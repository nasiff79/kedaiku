<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Belajar Bootstrap</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js"
        integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js"
        integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc"
        crossorigin="anonymous"></script>

        <link rel="stylesheet" href="/css/style.css">
</head>
<body>
    <!-- Image and text -->
    <nav class="navbar navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">
            <img src="/img/logo-120.png" width="30" height="30" class="d-inline-block align-top" alt="">
            KelasProgramming.com
            </a>
        </div>
    </nav>


        <!--hero area-->
    <div class="hero-area">

    <div class="container">
        <div class="row">
            <div class="col">
                <h1>Gallery</h1>
                  <p>Gallery of random images</p>
                 </div>
             </div>
         </div>
    </div>

    <div class="container mt-5">

        <!-- subtitle -->
        
        <div class="row">
            <div class="col text-center">
                <h3>Image grid</h3>
            </div>
        </div>

        <!-- row -->
        <div class="row">

        <?php foreach ($all_pekan as $pekan) : ?>
            <!--card1-->
            <div class="col-12 col-sm-6 col-md-4  col-lg-3 mt-3">
                <div class="card">
                    <img src="/img/<?php echo $pekan->nama_fail?>" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5> <?php echo $pekan->nama?> </h5>
                        <p class="card-text"><?php echo $pekan->keterangan?></p>
                    </div>
                </div>

            </div>
            <?php endforeach; ?>
        </div> <!-- row -->

        <!-- pagination -->
        <div class="row p-5">
            <div class="col-12">
        
                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-center">
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav>
        
            </div>
        </div><!-- /pagination -->

    </div>

    <footer class="text-center p-5">
        <p>Copyright Reserved &copy; 2021</p>
    
    </footer>


</body>
</html>