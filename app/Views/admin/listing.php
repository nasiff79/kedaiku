<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js"
        integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js"
        integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc"
        crossorigin="anonymous"></script>
    
    <link rel="stylesheet" href="/css/admin.css">
</head>
<body>

    <!-- Image and text -->
    <nav class="navbar navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">
                <img src="img/logo-120.png" width="30" height="30" class="d-inline-block align-top" alt="">
                KelasProgramming.com
            </a>
        </div>
    </nav>

    <div class="container mt-5">

    <?php if (isset($_SESSION['success'])) :?>
            <div class="row">
                <div class="col">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Success!</strong> New data has been added.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                </div>
                </div>
            </div>

            <?php endif; ?>

            <!--deleted alet--> 
            <?php if (isset($_SESSION['deleted'])) :?>
            <div class="row">
                <div class="col">
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <strong>Success!</strong> Data has been deleted.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                </div>
            </div>

            <?php endif; ?>



        <div class="row">
            <div class="col-12">
            <!-- <a href="/gambar/add" class="btn btn-sm btn-primary float-right">Add New</a> -->
                <a href="/gambar/add" class="btn btn-sm btn-primary float-end">Add New</a>
                <h3>Senarai Gambar</h3>
            </div>


            <div class="col-12">

                <table class="table table-striped table-sm"> 
                    <thead class="thead-dark">
                        <tr>
                            <th>ID</th>
                            <th>Gambar</th>
                            <th>Nama</th>
                            <th> </th>
                        </tr>
                    </thead>

                    <tbody>
    <?php foreach($gambar as $g) : ?>                    
                        <tr>
                            <td><?= $g['id'];?></td>
                            <td>
                            <img class="gambar-pekan" src="/img/<?= $g['nama_fail']?>" alt="">
                            </td>
                            <td>
                            <?= $g['nama']?>
                            </td>
                            <td>
                                <a href="/gambar/edit/<?= $g['id'];?>" class="btn btn-sm btn-primary">Edit</a>
                                <button href="/gambar/delete/<?= $g['id'];?>" onclick="confirm_delete( <?= $g['id'];?> )"  class="btn btn-sm btn-danger">Delete</button>

                            </td>
                        </tr>
    <?php endforeach; ?>


                    </tbody>
                </table>

                <!-- <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-center">
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav> -->
                <div id="my-pagination">
                    <?= $pager->links() ?>
                </div>


            </div>
        </div>
    </div>

    <footer class="text-center p-5">
        <p>Copyright Reserved &copy; 2021</p>
    
    </footer>

    <script>

        function confirm_delete( id ) {
            if ( confirm( 'Are you sure you want to delete record ID '+ id + '?' ) ) {
                window.location.href = '/gambar/delete/' + id;
            }
        }

</script>

    
</body>

</html>